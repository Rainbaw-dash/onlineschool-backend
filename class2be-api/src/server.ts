import express , {Express} from 'express'
import sequelizes from './db.config/db.config'
import cors from 'cors'
import userControler from './routes/User.auth'

const app:Express = express()
const PORT = 8000


const run = async() => {

  try {
    await sequelizes.authenticate()
        
    await sequelizes.sync()

    app.listen(PORT , () => console.log( 'Server start on ' , PORT))

  } catch (error) {
    
    console.log("FAQ");
    
  }

}


app.use(cors())
app.use(express.json())

app.use('/' , userControler) 


run().catch(e => console.log(e)) 