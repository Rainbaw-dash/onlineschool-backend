import { DataTypes, Model } from 'sequelize'
import sequelize from '../db.config/db.config'
import bcrypt from 'bcrypt'
import { nanoid } from 'nanoid'

interface IUser extends Model {
      id:number
      username :string
      password:string
      role:string
      token:string
      checkPassword:(password :string) => boolean
      generationToken : () => void
      toJSON : () => Omit<IUser , 'password'>
}

const UserModel = sequelize.define<IUser>('users' , {

      id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
      },
      username: {
            type:DataTypes.STRING,
            allowNull:false
      },
      password : {
            type:DataTypes.STRING,
            allowNull:false,
      },
      token:{
            type:DataTypes.STRING,
            allowNull:false,  
      },
      role:{
            type:DataTypes.STRING,
            allowNull:false 
      },
      
})

UserModel.beforeSave(async (user:IUser) => {

      if (user.changed('password')) {
            
        const salt = await bcrypt.genSalt(10);

        const hashedPassword = await bcrypt.hash(user.password, salt);
      
        user.password = hashedPassword;

      }
});

UserModel.prototype.checkPassword = async function (password :string)  {

      return bcrypt.compare(password, this.password);

}

UserModel.prototype.toJSON = function () {

      const values = Object.assign({}, this.get());
    
      delete values.password;
    
      return values;
};


UserModel.prototype.generationToken = async function() {

      this.token = nanoid()

}

export default UserModel